(ns groupon.ui
  (:use seesaw.core
        seesaw.graphics
        seesaw.color)
  (:require [groupon.core :refer :all])
  (:import [groupon.core Circle Point]))


(def circle-style (style :foreground java.awt.Color/DARK_GRAY
                       :stroke (stroke :width 3 :cap :round)))
(def point-style (style :foreground java.awt.Color/RED
                       :stroke (stroke :width 2 :cap :round)))

(def center (Point. 320 240))
(def acircle (Circle. center 100))

(defn draw-circle [g]
  (draw g (circle 320 240 100) circle-style))

(defn draw-single-point [^java.awt.Graphics2D g ^Point point]
  (draw g (circle (:x point) (:y point) 1) point-style))

(defn draw-points [^java.awt.Graphics2D g ls]
  (map #(draw-single-point g %) ls))

(defn paint-circle-and-points [^javax.swing.JComponent c ^java.awt.Graphics2D g]
  (do
    (draw-circle g)
    (loop [pts (take 100 (rand-points-stream-within acircle))]
      (when-not (empty? pts)
        (draw-single-point g (first pts))
        (recur (rest pts))))))

(defn -main[& args]
  (let [cvs (canvas :id :canvas :paint paint-circle-and-points)]
    (invoke-later
     (-> (frame :title "100 Points within a circle"
                :content cvs
                :minimum-size [640 :by 480]
                :on-close :dispose)
         pack!
         show!))))


  
