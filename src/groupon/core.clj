(ns groupon.core
  (:require [clojure.math.numeric-tower :refer [abs sqrt]]))

;; circle concepts for radius r
;; sqr(a) + sqr(b) = sqr(c)
;;   a <- pick random from 0 to r
;;   c <- pick random from a to r
;;   b <- sqrt( sqr(c) - sqr(b) )
;;
;;   x <- coinflip(+ | - ) a
;;   y <- coinflip(+ | - ) b

(defrecord Point [x y])
(defrecord Circle [center radius])

(defn- random-x [^ Circle circle] 
  "Random x coord within circle radius"
  (let [x (rand (:radius circle))
        negative? (zero? (rand-int 2))
        adjustment (if negative? -1 1)]
    (* adjustment x)))

(defn- random-r [x ^Circle circle]
  "Random point on the radius line, given a fixed x coord"
  (let [radius (:radius circle)
        posx (abs x)]
    (+ (rand (- radius posx)) posx)))

(defn- calculate-y 
  "Calculate the Y coordinate given a fixed x coord and r radius length"
  [x r]
  (let [negative? (zero? (rand-int 2))
        adjustment (if negative? -1 1)
        sqr #(* % %)]
    (* adjustment (sqrt (abs (- (sqr r) (sqr x)))))))

(defn- translate-position [^Point pos ^Circle circle]
  "Translates the position from 0, 0 center to the circle center"
  (Point. (+ (:x pos) (:x (:center circle)))
             (+ (:y pos) (:y (:center circle)))))

(defn- rand-point-within [^Circle circle]
  "Returns One random point within the circle"
  (let [x (random-x circle)
        r (random-r x circle)
        y (calculate-y x r)]
    (translate-position (Point. x y) circle)))

(defn rand-points-stream-within [^Circle circle]
  "Returns a lazy stream of points within the circle"
  (lazy-seq (cons (rand-point-within circle)
                  (rand-points-stream-within circle))))



(defn distance-from
  "Pythagoras to calculate distance"
  [^Point p1 ^Point p2]
  (let [a (abs (- (abs (:x p2)) (abs (:x p1))))
        b (abs (- (abs (:y p2)) (abs (:y p1))))]
    (sqrt (+ (* a a) (* b b)))))


(take 10 (rand-points-stream-within  (Circle. (Point. 0 0) 5)))

