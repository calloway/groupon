(ns groupon.core-test
  (:require [clojure.test :refer :all]
            [groupon.core :refer :all]
            [groupon.ui :refer :all])
  (:import [groupon.core Circle Point]))

(def circle-radius1 (Circle. (Point. 0 0) 1))
(def pts-circle-radius1 (take 10000 (rand-points-stream-within circle-radius1)))

(def circle-radius10 (Circle. (Point. -123 -233) 10))
(def pts-circle-radius10 (take 10000 (rand-points-stream-within circle-radius10)))

(deftest pts-within-circle-test
  (testing "testing 10000 points within circle radius"
    (is (empty?
         (filter #(> (distance-from % (:center circle-radius1)) (:radius circle-radius1)) pts-circle-radius1)))

    (is (= 10000
         (count (filter #(<= (distance-from % (:center circle-radius10)) (:radius circle-radius10)) pts-circle-radius10))))))
